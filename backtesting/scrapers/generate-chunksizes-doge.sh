awk -F ',' '{OFS=","; print $1,$5,$5}' doge-blocks.csv \
| ../../implementation-c/bin/aggregate -windowlength 10 \
| awk -F ',' 'NR>1 {OFS=","; print NR-2,0,$15}' \
> chunksizes-doge.csv
