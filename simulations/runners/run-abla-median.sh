#!/bin/bash

# output file
outfile=$1

# scenario test function to load
source $2

# algo args
excessiveblocksize=$3
n0=$4
alpha0_xB7=$5
windowLength=$6

tmpsim=$(mktemp)
tmpfifo=$(mktemp -u); mkfifo $tmpfifo

( echo 0,0,0,0; cat $tmpfifo ) | \
stdbuf -i0 -o0 awk -F ',' '{ print $3 };' | \
testfun | \
stdbuf -i0 -o0 ../../implementation-c/bin/abla-median -excessiveblocksize $excessiveblocksize -ablaconfig $n0,$alpha0_xB7,$windowLength | \
tee $tmpsim >$tmpfifo

rm $tmpfifo

tmpeb=$(mktemp)
tmpcf=$(mktemp)
tmpnb=$(mktemp)


awk -F ',' '{OFS=",";print $1,$2,$3}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 > $tmpeb
awk -F ',' '{OFS=",";print $1,$2,$4}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpcf
sed -i '1 s/openExcessiveBlockSize/blockSizeLimit/' $tmpeb
sed -i '1 s/closeExcessiveBlockSize/blockSizeLimit/' $tmpeb
sed -i '1 s/openExcessiveBlockSize/medianBlockSize/' $tmpcf
sed -i '1 s/closeExcessiveBlockSize/medianBlockSize/' $tmpcf

join --header  --nocheck-order -t, -1 1 -2 1 $tmpeb $tmpcf >$outfile

rm $tmpsim
rm $tmpeb
rm $tmpcf
rm $tmpnb
