# Stakeholder Responses & Statements

## Table of Contents

- [Request For Approval Responses](#request-for-approval-responses)
  - [Nodes](#nodes)
  - [Wallets](#wallets)
  - [Projects](#projects)
  - [Industry](#industry)
- [Statements](#statements)
  - [Approve](#approve)
  - [Neutral](#neutral)
  - [Disapprove](#disapprove)

## Request For Approval Responses

The CHIP maintainer has contacted the following stakeholders for approval of this specification, using the following request template:

<details>

<summary><strong>Standard Approval Request Template</strong></summary>

<blockquote>

**Subject:** Nov 14 deadline: Adaptive Blocksize Limit Algorithm in 2024 BCH upgrade

Hello,

This is a formal request for approval from {{Organization}} regarding the May 2024 upgrade of Bitcoin Cash (BCH).

CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash has achieved the Cash Improvement Proposal (CHIP) milestones required to be locked in on November 15, 2023 and activated on May 15, 2024.

Support for the Adaptive Blocksize Limit Algorithm upgrade is currently available in the following open-source node software:

- Bitcoin Cash Node (BCHN): https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1782

The CHIP contributors believe {{Organization}} is an important stakeholder in the Bitcoin Cash ecosystem.
**Please respond to the following question by 12:00 UTC on November 14, 2023. Non-responses will be considered "Abstain (Neutral)".**

> Does {{Organization}} approve of activating CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash in the May 2024 Upgrade to Bitcoin Cash (BCH)?
> 
> - Yes (Approve)
> - No (Disapprove), or
> - Abstain (Neutral)
> 
> To make a public statement explaining this decision, please provide it here (required for disapproval):

For reference:

- CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash: https://gitlab.com/0353F40E/ebaa
- Stakeholders & Responses: https://gitlab.com/0353F40E/ebaa/-/blob/main/stakeholders.md

Please let me know if you have any questions, and note that many common questions have been addressed in the CHIP:

- Could some adversary game the algorithm? https://gitlab.com/0353F40E/ebaa#spam-attack
- Could the algorithm be too fast? https://gitlab.com/0353F40E/ebaa#algorithm-too-fast
- Could the algorithm be too slow? https://gitlab.com/0353F40E/ebaa#algorithm-too-slow
- What about alternative X? https://gitlab.com/0353F40E/ebaa#evaluation-of-alternatives

Thank you,

bitcoincashautist  
Adaptive Blocksize Limit Algorithm CHIP maintainer  
Independent contributor (https://gitlab.com/A60AB5450353F40E)

</blockquote>

</details>

Responses have been logged by the maintainer and are presented below:

### Nodes

Responses from open source node implementations that support Bitcoin Cash.

| Node                                                    | As of Version | Approve | Disapprove | Neutral | Pending |
| ------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [BCHD](https://bchd.cash/)                              |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcoin Cash Node](https://bitcoincashnode.org/)       |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Unlimited](https://www.bitcoinunlimited.info/) |   `b6649f98`  |         |            |         |    ☐    |
| [Bitcoin Verde](https://bitcoinverde.org/)              |   `b6649f98`  |         |            |         |    ☐    |
| [Flowee](https://flowee.org/products/hub/)              |   `b6649f98`  |         |            |         |    ☐    |
| [Knuth](https://kth.cash/)                              |   `b6649f98`  |         |            |         |    ☐    |

### Wallets

Responses from wallets that support Bitcoin Cash.

| Wallet                                                           | As of Version | Approve | Disapprove | Neutral | Pending |
| ---------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [Atomic Wallet](https://atomicwallet.io/)                        |   `b6649f98`  |         |            |         |         |
| [`bchwallet`](https://github.com/gcash/bchwallet)                |   `b6649f98`  |         |            |         |         |
| [Bitcoin.com Wallet](https://wallet.bitcoin.com/)                |   `b6649f98`  |         |            |         |         |
| [BitPay Wallet](https://bitpay.com/wallet)                       |   `b6649f98`  |         |            |         |         |
| [Bitpie](https://bitpie.com/)                                    |   `b6649f98`  |         |            |         |         |
| [BlockParty Wallet](https://blockparty.sh/)                      |   `b6649f98`  |         |            |         |         |
| [BRD](https://brd.com/)                                          |   `b6649f98`  |         |            |         |         |
| [CashAddress.org](https://cashaddress.org/)                      |   `b6649f98`  |         |            |         |         |
| [Cashonize](https://cashonize.com/)                              |   `b6649f98`  |    ☑    |            |         |         |
| [Cashual Wallet](https://gitlab.com/monsterbitar/cashual-wallet) |   `b6649f98`  |         |            |         |         |
| [Cake Wallet](https://cakewallet.com/)                           |   `b6649f98`  |         |            |         |         |
| [Coinbase Wallet](https://www.coinbase.com/wallet)               |   `b6649f98`  |         |            |         |         |
| [Coin Wallet](https://coin.space/)                               |   `b6649f98`  |         |            |         |         |
| [Coinomi](https://www.coinomi.com/)                              |   `b6649f98`  |         |            |         |         |
| [Cwallet](https://cwallet.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Edge](https://edge.app/)                                        |   `b6649f98`  |         |            |         |         |
| [Electron Cash](https://electroncash.org/)                       |   `b6649f98`  |         |            |    -    |         |
| [Exodus](https://www.exodus.com/)                                |   `b6649f98`  |         |            |         |         |
| [Flowee Pay](https://flowee.org/products/pay/)                   |   `b6649f98`  |         |            |         |         |
| [Guarda](https://guarda.com/)                                    |   `b6649f98`  |         |            |         |         |
| [imToken](https://token.im/)                                     |   `b6649f98`  |         |            |         |         |
| [Jaxx Liberty](https://jaxx.io/)                                 |   `b6649f98`  |         |            |         |         |
| [KeepKey](https://shapeshift.com/keepkey)                        |   `b6649f98`  |         |            |         |         |
| [Ledger](https://www.ledger.com/)                                |   `b6649f98`  |         |            |         |         |
| [Lumi](https://lumiwallet.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Melis](https://www.melis.io/)                                   |   `b6649f98`  |         |            |         |         |
| [Metamask](https://metamask.io/)                                 |   `b6649f98`  |         |            |         |         |
| [Mobi](https://www.mobi.me/)                                     |   `b6649f98`  |         |            |         |         |
| [Neutrino](https://neutrino.cash/)                               |   `b6649f98`  |         |            |         |         |
| [Payperless](https://www.payperless.com/)                        |   `b6649f98`  |         |            |         |         |
| [Paytaca Wallet](https://www.paytaca.com/)                       |   `b6649f98`  |         |            |         |    ☐    |
| [Satochip](https://satochip.io/)                                 |   `b6649f98`  |         |            |         |         |
| [Selene](https://selene.cash/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [SimpleHold](https://simplehold.io/)                             |   `b6649f98`  |         |            |         |         |
| [Stack Wallet](https://stackwallet.com/)                         |   `b6649f98`  |         |            |         |    ☐    |
| [StrongCoin](https://strongcoin.com/)                            |   `b6649f98`  |         |            |         |         |
| [Trezor](https://trezor.io/)                                     |   `b6649f98`  |         |            |         |         |
| [Trust Wallet](https://trustwallet.com/)                         |   `b6649f98`  |         |            |         |         |
| [Trustee](https://trusteeglobal.com/)                            |   `b6649f98`  |         |            |         |         |
| [Unstoppable Wallet](https://unstoppable.money/)                 |   `b6649f98`  |         |            |         |         |
| [Verde Wallet](https://github.com/SoftwareVerde/verde-wallet)    |   `b6649f98`  |         |            |         |         |
| [ViaWallet](https://viawallet.com/)                              |   `b6649f98`  |         |            |         |         |
| [West Wallet](https://westwallet.io/)                            |   `b6649f98`  |         |            |         |         |
| [WH Cypher](https://www.whalesheaven.com/whcypher)               |   `b6649f98`  |         |            |         |         |
| [Zapit](https://www.zapit.io/)                                   |   `b6649f98`  |         |            |         |    ☐    |

### Projects

Responses from open source libraries, indexers, educational resources, and community initiatives.

| Project                                                           | As of Version | Approve | Disapprove | Neutral | Pending |
| ----------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [Accept Bitcoin Cash Initiative](https://acceptbitcoin.cash/)     |   `b6649f98`  |         |            |         |         |
| [ActorForth](https://github.com/ActorForth/ActorForth)            |   `b6649f98`  |         |            |         |    ☐    |
| [Allodium](https://allodium.is/)                                  |   `b6649f98`  |    ☑    |            |         |         |
| [AnyHedge](https://anyhedge.com/)                                 |   `b6649f98`  |    ☑    |            |         |         |
| [awesomebitcoin.cash](https://awesomebitcoin.cash/)               |   `b6649f98`  |    ☑    |            |         |         |
| [bch.info](https://bch.info/)                                     |   `b6649f98`  |    ☑    |            |         |         |
| [`bch-rpc-explorer`](https://github.com/sickpig/bch-rpc-explorer) |   `b6649f98`  |    ☑    |            |         |         |
| [BCH DevSuite](https://devsuite.actorforth.org/)                  |   `b6649f98`  |         |            |         |    ☐    |
| [BCH Explorer](https://explorer.melroy.org/)                      |   `b6649f98`  |    ☑    |            |         |         |
| [BCH Guru](https://bch.guru/)                                     |   `b6649f98`  |         |            |         |    ☐    |
| [`bchtipbot`](https://github.com/merc1er/bchtipbot)               |   `b6649f98`  |         |            |         |    ☐    |
| [BCHMempool.cash](https://bchmempool.cash/)                       |   `b6649f98`  |         |            |         |         |
| [bestbchwallets.com](https://www.bestbchwallets.com/)             |   `b6649f98`  |         |            |         |         |
| [Bitauth IDE](https://ide.bitauth.com)                            |   `b6649f98`  |         |            |         |         |
| [BitcartCC](https://bitcartcc.com/)                               |   `b6649f98`  |         |            |         |         |
| [Bitcash](https://github.com/pybitcash/bitcash)                   |   `b6649f98`  |         |            |         |         |
| [Bitcoin Cash Argentina](https://bitcoincashargentina.com/)       |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Cash Foundation](https://bitcoincashfoundation.org/)     |   `b6649f98`  |    ☑    |            |         |         |
| [Bitcoin Cash Hangout](https://rss.com/podcasts/fiendishcrypto/)  |   `b6649f98`  |         |            |         |         |
| [Bitcoin Cash Podcast](https://bitcoincashpodcast.com/)           |   `b6649f98`  |    ☑    |            |         |         |
| [BitcoinCash.org](https://bitcoincash.org/)                       |   `b6649f98`  |         |            |         |         |
| [BitcoinCashResearch.org](https://bitcoincashresearch.org/)       |   `b6649f98`  |         |            |         |    ☐    |
| [BitcoinCashSite.com](https://bitcoincashsite.com/)               |   `b6649f98`  |         |            |         |         |
| [BitcoinFees.cash](https://bitcoinfees.cash/)                     |   `b6649f98`  |         |            |         |         |
| [Bitcore](https://github.com/bitpay/bitcore)                      |   `b6649f98`  |         |            |         |         |
| [Bmap.app](https://bmap.app/)                                     |   `b6649f98`  |         |            |         |         |
| [CashAccount.info](https://www.cashaccount.info/)                 |   `b6649f98`  |         |            |         |         |
| [CashChannels.org](http://cashchannels.org/)                      |   `b6649f98`  |         |            |         |         |
| [CashFusion.org](https://cashfusion.org/)                         |   `b6649f98`  |         |            |         |         |
| [CashFusion Red Team](https://fusionstats.redteam.cash/)          |   `b6649f98`  |         |            |         |         |
| [CashID.info](https://www.cashid.info/)                           |   `b6649f98`  |         |            |         |         |
| [CashNinja](https://bch.ninja/)                                   |   `b6649f98`  |    ☑    |            |         |         |
| [CashNinjas](https://ninjas.cash/)                                |   `b6649f98`  |         |            |         |    ☐    |
| [Cashnodes.io](https://cashnodes.io/)                             |   `b6649f98`  |         |            |         |         |
| [CashScript](https://cashscript.org/)                             |   `b6649f98`  |         |            |         |    ☐    |
| [CashShuffle](https://cashshuffle.com/)                           |   `b6649f98`  |         |            |         |         |
| [CashTags](https://tags.infra.cash/)                              |   `b6649f98`  |         |            |         |         |
| [Chaingraph](https://chaingraph.cash/)                            |   `b6649f98`  |         |            |         |         |
| [Coin Center](https://www.coincenter.org/)                        |   `b6649f98`  |         |            |         |         |
| [Developers.Cash](https://developers.cash/)                       |   `b6649f98`  |         |            |         |         |
| [Documentation.cash](https://documentation.cash/)                 |   `b6649f98`  |         |            |         |         |
| [eatBCH](https://eatbch.org/)                                     |   `b6649f98`  |         |            |         |         |
| [Fex.cash](https://fex.cash/)                                     |   `b6649f98`  |         |            |         |    ☐    |
| [Flipstarter.cash](https://flipstarter.cash/)                     |   `b6649f98`  |         |            |         |         |
| [Flipstarter.me](https://flipstarter.me/)                         |   `b6649f98`  |         |            |         |         |
| [Flowee Products](https://flowee.org/products/)                   |   `b6649f98`  |         |            |         |         |
| [Fountainhead Cash](https://fountainhead.cash/)                   |   `b6649f98`  |         |            |         |         |
| [Fulcrum](https://github.com/cculianu/Fulcrum)                    |   `b6649f98`  |    ☑    |            |         |         |
| [GiveCrypto](https://givecrypto.org/)                             |   `b6649f98`  |         |            |         |         |
| [Googol.cash Testnet Faucet](https://tbch4.googol.cash/)          |   `b6649f98`  |         |            |         |         |
| [Jedex](https://github.com/bitjson/jedex)                         |   `b6649f98`  |         |            |         |         |
| [Johoe's Mempool Statistics](https://jochen-hoenicke.de/queue/)   |   `b6649f98`  |         |            |         |         |
| [HelpMe.Cash](https://helpme.cash/)                               |   `b6649f98`  |         |            |         |         |
| [Knuth Libraries](https://github.com/k-nuth)                      |   `b6649f98`  |         |            |         |         |
| [Libauth](https://libauth.org/)                                   |   `b6649f98`  |         |            |         |         |
| [Mainnet.cash](https://mainnet.cash/)                             |   `b6649f98`  |    ☑    |            |         |         |
| [Meep](https://github.com/gcash/meep)                             |   `b6649f98`  |         |            |         |         |
| [One.Surgery](https://one.surgery/)                               |   `b6649f98`  |         |            |         |         |
| [Oracles.cash](https://oracles.cash/)                             |   `b6649f98`  |    ☑    |            |         |         |
| [PayButton](https://paybutton.org/)                               |   `b6649f98`  |         |            |         |         |
| [Permissionless Software Foundation](https://psfoundation.cash/)  |   `b6649f98`  |         |            |    -    |         |
| [Pos.cash](https://github.com/softwareverde/pos-cash)             |   `b6649f98`  |         |            |         |         |
| [python-bitcoincash](https://pypi.org/project/bitcoincash/)       |   `b6649f98`  |         |            |         |         |
| [rbch](https://cran.r-project.org/package=rbch)                   |   `b6649f98`  |         |            |         |         |
| [Rostrum](https://gitlab.com/bitcoinunlimited/rostrum)            |   `b6649f98`  |         |            |         |    ☐    |
| [rust-bitcoincash](https://crates.io/crates/bitcoincash)          |   `b6649f98`  |         |            |         |         |
| [ScanToPay.cash](https://scantopay.cash/)                         |   `b6649f98`  |         |            |         |         |
| [Signup](https://signup.cash/)                                    |   `b6649f98`  |         |            |         |         |
| [SimpleLedger.info](https://simpleledger.info/)                   |   `b6649f98`  |         |            |         |         |
| [slp.dev](https://slp.dev/)                                       |   `b6649f98`  |         |            |         |         |
| [SmartBCH](https://smartbch.org/)                                 |   `b6649f98`  |         |            |         |         |
| [SmartScan](https://www.smartscan.cash/)                          |   `b6649f98`  |         |            |         |         |
| [Spedn](https://spedn.pl/)                                        |   `b6649f98`  |         |            |         |         |
| [Watchtower](https://watchtower.cash/)                            |   `b6649f98`  |         |            |         |         |
| [Where2.cash](https://www.where2.cash/)                           |   `b6649f98`  |         |            |         |         |

### Industry

Responses from exchanges, miners, services, and other businesses.

| Organization                                                   | As of Version | Approve | Disapprove | Neutral | Pending |
| -------------------------------------------------------------- | :-----------: | :-----: | :--------: | :-----: | :-----: |
| [1BCH.com](https://1bch.com/)                                  |   `b6649f98`  |         |            |         |         |
| [365 Cash](https://365cash.co/)                                |   `b6649f98`  |         |            |         |         |
| [Alagoria](https://alagoria.com/)                              |   `b6649f98`  |         |            |         |         |
| [Alfacoins](https://www.alfacoins.com/)                        |   `b6649f98`  |         |            |         |         |
| [Alfatop](https://alfa.top/)                                   |   `b6649f98`  |         |            |         |         |
| [Antpool](https://www.antpool.com/)                            |   `b6649f98`  |         |            |         |         |
| [Anyonecanpay.me](https://anyonecanpay.me)                     |   `b6649f98`  |         |            |         |         |
| [Anypay](https://anypayx.com/)                                 |   `b6649f98`  |         |            |         |         |
| [ArzPaya](https://arzpaya.com/)                                |   `b6649f98`  |         |            |         |         |
| [AscendEX](https://ascendex.com/)                              |   `b6649f98`  |         |            |         |         |
| [AtomicDEX](https://atomicdex.io/)                             |   `b6649f98`  |         |            |         |         |
| [B2BX](https://www.b2bx.exchange/)                             |   `b6649f98`  |         |            |         |         |
| [B2BinPay](https://b2binpay.com/)                              |   `b6649f98`  |         |            |         |         |
| [BCH BULL](https://bchbull.com/)                               |   `b6649f98`  |    ☑    |            |         |         |
| [BCH.games](https://bch.games/)                                |   `b6649f98`  |         |            |         |         |
| [bch.domains](https://bch.domains/)                            |   `b6649f98`  |         |            |         |         |
| [BenSwap](https://benswap.cash/)                               |   `b6649f98`  |         |            |         |         |
| [BestRate](https://bestrate.org/)                              |   `b6649f98`  |         |            |         |         |
| [Binance](https://www.binance.com/)                            |   `b6649f98`  |         |            |         |         |
| [BiggestLab.io](https://biggestlab.io)                         |   `b6649f98`  |         |            |         |         |
| [Bit.com](https://www.bit.com/)                                |   `b6649f98`  |         |            |         |         |
| [Bitauth.com](https://bitauth.com)                             |   `b6649f98`  |         |            |         |         |
| [Bitcoin.com](https://bitcoin.com/)                            |   `b6649f98`  |         |            |         |         |
| [Bitcoin.de](https://www.bitcoin.de/)                          |   `b6649f98`  |         |            |         |         |
| [BitcoinVN](https://bitcoinvn.io/)                             |   `b6649f98`  |         |            |         |         |
| [Bitdeer](https://www.bitdeer.com/)                            |   `b6649f98`  |         |            |         |         |
| [Bitexen](https://www.bitexen.com/)                            |   `b6649f98`  |         |            |         |         |
| [BitFlyer](https://bitflyer.com/)                              |   `b6649f98`  |         |            |         |         |
| [Bitfinex](https://www.bitfinex.com/)                          |   `b6649f98`  |         |            |         |         |
| [BitGlobal](https://www.bitglobal.com/)                        |   `b6649f98`  |         |            |         |         |
| [Bithumb](https://www.bithumb.com/)                            |   `b6649f98`  |         |            |         |         |
| [BitMEX](https://www.bitmex.com/)                              |   `b6649f98`  |         |            |         |         |
| [Bitpanda](https://www.bitpanda.com/)                          |   `b6649f98`  |         |            |         |         |
| [BitPay](https://bitpay.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Bitso](https://bitso.com/)                                    |   `b6649f98`  |         |            |         |         |
| [Bitstamp](https://www.bitstamp.net/)                          |   `b6649f98`  |         |            |         |         |
| [Bittrex](https://bittrex.com/)                                |   `b6649f98`  |         |            |         |         |
| [Bitvavo](https://bitvavo.com/)                                |   `b6649f98`  |         |            |         |         |
| [BitYard](https://www.bityard.com/)                            |   `b6649f98`  |         |            |         |         |
| [BKEX](https://www.bkex.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Blockchain Poker](https://blockchain.poker/)                  |   `b6649f98`  |         |            |         |         |
| [Blockchain.com](https://blockchain.com/)                      |   `b6649f98`  |         |            |         |         |
| [Blockchair](https://blockchair.com/)                          |   `b6649f98`  |         |            |         |         |
| [BlockNG](https://blockng.money/)                              |   `b6649f98`  |         |            |         |         |
| [Blockonomics](https://www.blockonomics.co/)                   |   `b6649f98`  |         |            |         |         |
| [BTC.com](https://btc.com/)                                    |   `b6649f98`  |         |            |         |         |
| [BTCBOX](https://www.btcbox.co.jp/)                            |   `b6649f98`  |         |            |         |         |
| [BTCPOP](https://btcpop.co/)                                   |   `b6649f98`  |         |            |         |         |
| [Buy Hodl Sell](https://buyhodlsell.com/)                      |   `b6649f98`  |         |            |         |         |
| [Buda.com](https://www.buda.com/)                              |   `b6649f98`  |         |            |         |         |
| [Bybit](https://www.bybit.com/)                                |   `b6649f98`  |         |            |         |         |
| [Centre](https://www.centre.io/)                               |   `b6649f98`  |         |            |         |         |
| [CEX.IO](https://cex.io/)                                      |   `b6649f98`  |         |            |         |         |
| [Chaintip](https://www.chaintip.org/)                          |   `b6649f98`  |         |            |         |         |
| [Changelly](https://changelly.com/)                            |   `b6649f98`  |         |            |         |         |
| [ChangeNOW](https://changenow.io/)                             |   `b6649f98`  |         |            |         |         |
| [CheapAir](https://www.cheapair.com/)                          |   `b6649f98`  |         |            |         |         |
| [CheckBook.cash](https://checkbook.cash/)                      |   `b6649f98`  |         |            |         |         |
| [Circle](https://www.circle.com/)                              |   `b6649f98`  |         |            |         |         |
| [Clementine's Nightmare](https://clementinesnightmare.io/)     |   `b6649f98`  |         |            |         |         |
| [Coin ATM Radar](https://coinatmradar.com/)                    |   `b6649f98`  |         |            |         |         |
| [Coin Dance](https://cash.coin.dance/)                         |   `b6649f98`  |         |            |         |         |
| [Coinbase](https://www.coinbase.com/)                          |   `b6649f98`  |         |            |         |         |
| [Coinbooth](https://coinbooth.io/)                             |   `b6649f98`  |         |            |         |         |
| [CoinCarp](https://www.coincarp.com/)                          |   `b6649f98`  |         |            |         |         |
| [CoinChange](https://coinchange.to/)                           |   `b6649f98`  |         |            |         |         |
| [Coincheck](https://coincheck.com/)                            |   `b6649f98`  |         |            |         |         |
| [CoinCodex](https://coincodex.com/)                            |   `b6649f98`  |         |            |         |         |
| [CoinCola](https://www.coincola.com/)                          |   `b6649f98`  |         |            |         |         |
| [CoinEx](https://www.coinex.com/)                              |   `b6649f98`  |         |            |         |         |
| [CoinFalcon](https://coinfalcon.com/)                          |   `b6649f98`  |         |            |         |         |
| [CoinGate](https://coingate.com/)                              |   `b6649f98`  |         |            |         |         |
| [CoinGecko](https://www.coingecko.com/)                        |   `b6649f98`  |         |            |         |         |
| [CoinGigs](https://coingigs.net/)                              |   `b6649f98`  |         |            |         |         |
| [Coinify](https://www.coinify.com/)                            |   `b6649f98`  |         |            |         |         |
| [CoinLedger](https://coinledger.io/)                           |   `b6649f98`  |         |            |         |         |
| [CoinLoan](https://coinloan.io/)                               |   `b6649f98`  |         |            |         |         |
| [Coinone](https://coinone.co.kr/)                              |   `b6649f98`  |         |            |         |         |
| [CoinPayments](https://www.coinpayments.net/)                  |   `b6649f98`  |         |            |         |         |
| [Coinrabbit](https://coinrabbit.io/)                           |   `b6649f98`  |         |            |         |         |
| [CoinRemitter](https://coinremitter.com/)                      |   `b6649f98`  |         |            |         |         |
| [CoinSalad](https://coinsalad.com/)                            |   `b6649f98`  |         |            |         |         |
| [CoinStats](https://coinstats.app/)                            |   `b6649f98`  |         |            |         |         |
| [CoinSwitch](https://coinswitch.co/)                           |   `b6649f98`  |         |            |         |         |
| [CoinTracker](https://www.cointracker.io/)                     |   `b6649f98`  |         |            |         |         |
| [CoinTracking](https://cointracking.info/)                     |   `b6649f98`  |         |            |         |         |
| [Crex24](https://crex24.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Crypto.com](https://crypto.com/)                              |   `b6649f98`  |         |            |         |         |
| [CryptoCompare](https://www.cryptocompare.com/)                |   `b6649f98`  |         |            |         |         |
| [CryptoKnights.games](https://cryptoknights.games)             |   `b6649f98`  |         |            |         |         |
| [CryptoTaxCalculator](https://cryptotaxcalculator.io/)         |   `b6649f98`  |         |            |         |         |
| [CryptoWoo](https://www.cryptowoo.com/)                        |   `b6649f98`  |         |            |         |         |
| [Detoken](https://detoken.net/)                                |   `b6649f98`  |         |            |         |         |
| [Digital Currency Group](https://dcg.co/)                      |   `b6649f98`  |         |            |         |         |
| [eGifter](https://www.egifter.com/)                            |   `b6649f98`  |         |            |         |         |
| [eToro](https://www.etoro.com/)                                |   `b6649f98`  |         |            |         |         |
| [EmberSwap](https://www.emberswap.com/)                        |   `b6649f98`  |         |            |         |         |
| [Enter the Sphere](https://enter-the-sphere.com/)              |   `b6649f98`  |         |            |         |         |
| [EXMO](https://exmo.com/)                                      |   `b6649f98`  |         |            |         |         |
| [F2Pool](https://www.f2pool.com/)                              |   `b6649f98`  |         |            |         |         |
| [FTX](https://ftx.us/)                                         |   `b6649f98`  |         |            |         |         |
| [Foundry](https://foundrydigital.com/)                         |   `b6649f98`  |         |            |         |         |
| [FullStack.Cash](https://fullstack.cash/)                      |   `b6649f98`  |         |            |         |         |
| [Gate.io](https://gate.io/)                                    |   `b6649f98`  |         |            |         |         |
| [Gemini](https://www.gemini.com/)                              |   `b6649f98`  |         |            |         |         |
| [General Protocols](https://generalprotocols.com)              |   `b6649f98`  |    ☑    |            |         |         |
| [Genesis Block](https://genesisblockhk.com/)                   |   `b6649f98`  |         |            |         |         |
| [Genesis Trading](https://genesistrading.com/)                 |   `b6649f98`  |         |            |         |         |
| [Goblins.cash](https://goblins.cash/)                          |   `b6649f98`  |         |            |         |         |
| [GoCrypto](https://gocrypto.com/)                              |   `b6649f98`  |         |            |         |         |
| [GoUrl.io](https://gourl.io/)                                  |   `b6649f98`  |         |            |         |         |
| [Grayscale Investments](https://grayscale.com/)                |   `b6649f98`  |         |            |         |         |
| [HereBets.com](https://herebets.com/)                          |   `b6649f98`  |         |            |         |         |
| [HitBTC](https://hitbtc.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Hop.cash](https://hop.cash/)                                  |   `b6649f98`  |         |            |         |         |
| [Honk World](https://honk.cash/)                               |   `b6649f98`  |         |            |         |         |
| [HQ Digital](https://www.hqdigital.com/)                       |   `b6649f98`  |         |            |         |         |
| [Huobi](https://www.huobi.com/)                                |   `b6649f98`  |         |            |         |         |
| [Independent Reserve](https://www.independentreserve.com/)     |   `b6649f98`  |         |            |         |         |
| [Indodax](https://indodax.com/)                                |   `b6649f98`  |         |            |         |         |
| [Joystick.club](https://smart.joystick.club/)                  |   `b6649f98`  |         |            |         |         |
| [Juungle](https://www.juungle.net/)                            |   `b6649f98`  |         |            |         |         |
| [Karsha Exchange](https://karsha.biz/)                         |   `b6649f98`  |         |            |         |         |
| [Keys4Coins](https://www.keys4coins.com/)                      |   `b6649f98`  |         |            |         |         |
| [Koinly](https://koinly.io/)                                   |   `b6649f98`  |         |            |         |         |
| [Korbit](https://www.korbit.co.kr/)                            |   `b6649f98`  |         |            |         |         |
| [Kraken](https://kraken.com/)                                  |   `b6649f98`  |         |            |         |         |
| [KuCoin](https://www.kucoin.com/)                              |   `b6649f98`  |         |            |         |         |
| [Launchpad IP](https://launchpadip.com/)                       |   `b6649f98`  |         |            |         |         |
| [Lazyfox.io](https://lazyfox.io/)                              |   `b6649f98`  |         |            |         |         |
| [LBank](https://www.lbank.info/)                               |   `b6649f98`  |         |            |         |         |
| [LetsExchange](https://letsexchange.io/)                       |   `b6649f98`  |         |            |         |         |
| [Liquid](https://www.liquid.com/)                              |   `b6649f98`  |         |            |         |         |
| [Living Room of Satoshi](https://www.livingroomofsatoshi.com/) |   `b6649f98`  |         |            |         |         |
| [LocalCryptos](https://localcryptos.com/)                      |   `b6649f98`  |         |            |         |         |
| [Luno](https://www.luno.com/)                                  |   `b6649f98`  |         |            |         |         |
| [MarketCap.cash](https://www.marketcap.cash/)                  |   `b6649f98`  |         |            |         |         |
| [Matrixport](https://www.matrixport.com/)                      |   `b6649f98`  |         |            |         |         |
| [Memo Technology, Inc.](https://memo.cash/)                    |   `b6649f98`  |         |            |         |         |
| [Member.cash](https://member.cash/)                            |   `b6649f98`  |         |            |         |         |
| [Menufy](https://www.menufy.com/)                              |   `b6649f98`  |         |            |         |         |
| [Mercuryo](https://mercuryo.io/)                               |   `b6649f98`  |         |            |         |         |
| [MEXC](https://www.mexc.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Mining-Dutch](https://www.mining-dutch.nl/)                   |   `b6649f98`  |         |            |         |         |
| [MistSwap](https://mistswap.fi/)                               |   `b6649f98`  |         |            |         |         |
| [MoonPay](https://www.moonpay.com/)                            |   `b6649f98`  |         |            |         |         |
| [Mullvad VPN](https://mullvad.net/)                            |   `b6649f98`  |         |            |         |         |
| [N.exchange](https://n.exchange/)                              |   `b6649f98`  |         |            |         |         |
| [Namecheap](https://www.namecheap.com/)                        |   `b6649f98`  |         |            |         |         |
| [Netcoins](https://netcoins.ca/)                               |   `b6649f98`  |         |            |         |         |
| [Nexo](https://nexo.io/)                                       |   `b6649f98`  |         |            |         |         |
| [NFTRarity.cash](https://nftrarity.cash/)                      |   `b6649f98`  |         |            |         |         |
| [Noise.app](https://noise.app/)                                |   `b6649f98`  |         |            |         |         |
| [Noise.cash](https://noise.cash/)                              |   `b6649f98`  |         |            |         |         |
| [NowPayments](https://nowpayments.io/)                         |   `b6649f98`  |         |            |         |         |
| [Oasis](https://oasis.cash/)                                   |   `b6649f98`  |         |            |         |         |
| [OKX](https://www.okx.com/)                                    |   `b6649f98`  |         |            |         |         |
| [Overbit](https://www.overbit.com/)                            |   `b6649f98`  |         |            |         |         |
| [Ownrare.art](https://ownrare.art/)                            |   `b6649f98`  |         |            |         |         |
| [Pagcripto](https://www.pagcripto.com.br/)                     |   `b6649f98`  |         |            |         |         |
| [Panmoni](https://panmoni.com/)                                |   `b6649f98`  |         |            |         |         |
| [Phemex](https://phemex.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Plisio](https://plisio.net/)                                  |   `b6649f98`  |         |            |         |         |
| [Poolin](https://www.poolin.com/)                              |   `b6649f98`  |         |            |         |         |
| [Poloniex](https://poloniex.com/)                              |   `b6649f98`  |         |            |         |         |
| [Pompler](https://pompler.com/)                                |   `b6649f98`  |         |            |         |         |
| [PrimeXBT](https://primexbt.com/)                              |   `b6649f98`  |         |            |         |         |
| [Prompt.Cash](https://prompt.cash/)                            |   `b6649f98`  |         |            |         |         |
| [ProBit](https://www.probit.com/)                              |   `b6649f98`  |         |            |         |         |
| [Prohashing](https://prohashing.com/)                          |   `b6649f98`  |         |            |         |         |
| [Publish0x](https://www.publish0x.com/)                        |   `b6649f98`  |         |            |         |         |
| [Purse](https://purse.io/)                                     |   `b6649f98`  |         |            |         |         |
| [Read.cash](https://read.cash/)                                |   `b6649f98`  |         |            |         |         |
| [Revoke](https://revoke.cash/)                                 |   `b6649f98`  |         |            |         |         |
| [Rocketr](https://rocketr.net/)                                |   `b6649f98`  |         |            |         |         |
| [SatoshiDice](https://satoshidice.com/)                        |   `b6649f98`  |         |            |         |         |
| [Satoshi's Angels](https://www.satoshisangels.com/)            |   `b6649f98`  |         |            |         |    ☐    |
| [SBICrypto Pool](https://sbicrypto.com/)                       |   `b6649f98`  |         |            |         |         |
| [ShapeShift](https://shapeshift.com/)                          |   `b6649f98`  |         |            |         |         |
| [SideShift.ai](https://sideshift.ai/)                          |   `b6649f98`  |         |            |         |         |
| [Sigmapool](https://sigmapool.com/)                            |   `b6649f98`  |         |            |         |         |
| [SimpleFX](https://simplefx.com/)                              |   `b6649f98`  |         |            |         |         |
| [SimpleSwap](https://simpleswap.io/)                           |   `b6649f98`  |         |            |         |         |
| [SmartSwap](https://smartswap.fi/)                             |   `b6649f98`  |         |            |         |         |
| [SolomonPierce](https://solomonpierce.com/)                    |   `b6649f98`  |         |            |         |         |
| [Sonar](https://sonar.cash/)                                   |   `b6649f98`  |         |            |         |         |
| [SouthXChange](https://southxchange.com/)                      |   `b6649f98`  |         |            |         |         |
| [Spice Token](https://spicetoken.org/)                         |   `b6649f98`  |         |            |         |         |
| [SpiderPool](https://www.spiderpool.com/)                      |   `b6649f98`  |         |            |         |         |
| [Stake](https://stake.com/)                                    |   `b6649f98`  |         |            |         |         |
| [StealthEX](https://stealthex.io/)                             |   `b6649f98`  |         |            |         |         |
| [Swapin](https://www.swapin.com/)                              |   `b6649f98`  |         |            |         |         |
| [SwapSpace](https://swapspace.co/)                             |   `b6649f98`  |         |            |         |         |
| [SwapSwop](https://swapswop.io/)                               |   `b6649f98`  |         |            |         |         |
| [Swapzone](https://swapzone.io/)                               |   `b6649f98`  |         |            |         |         |
| [Sweet](https://sweet.io/)                                     |   `b6649f98`  |         |            |         |         |
| [Switchere](https://switchere.com/)                            |   `b6649f98`  |         |            |         |         |
| [TANGOswap](https://tangoswap.cash/smart-swap)                 |   `b6649f98`  |         |            |         |         |
| [Tether](https://tether.to/)                                   |   `b6649f98`  |         |            |         |         |
| [THORSwap](https://thorswap.finance/)                          |   `b6649f98`  |         |            |         |         |
| [The Giving Block](https://thegivingblock.com/)                |   `b6649f98`  |         |            |         |         |
| [The Real Bitcoin Club](https://therealbitcoin.club/)          |   `b6649f98`  |         |            |         |         |
| [The Rock Trading](https://www.therocktrading.com/)            |   `b6649f98`  |         |            |         |         |
| [Trastra](https://trastra.com/)                                |   `b6649f98`  |         |            |         |         |
| [Travala](https://www.travala.com/)                            |   `b6649f98`  |         |            |         |         |
| [TxStreet](https://txstreet.com/)                              |   `b6649f98`  |         |            |         |         |
| [Unbanked](https://unbanked.com/)                              |   `b6649f98`  |         |            |         |         |
| [Uphold](https://uphold.com/)                                  |   `b6649f98`  |         |            |         |         |
| [Uptick](https://uptick.co/)                                   |   `b6649f98`  |         |            |         |         |
| [Verse](https://verse.bitcoin.com/)                            |   `b6649f98`  |         |            |         |         |
| [ViaBTC](https://www.viabtc.com/)                              |   `b6649f98`  |         |            |         |         |
| [Visionati](https://visionati.com/)                            |   `b6649f98`  |         |            |         |         |
| [WebMoney](https://bitcoincash.wmtransfer.com/)                |   `b6649f98`  |         |            |         |         |
| [WhalesHeaven](https://www.whalesheaven.com/)                  |   `b6649f98`  |         |            |         |         |
| [WhiteBIT](https://whitebit.com/)                              |   `b6649f98`  |         |            |         |         |
| [WOO](https://woo.org/)                                        |   `b6649f98`  |         |            |         |         |
| [Youhodler](https://www.youhodler.com/)                        |   `b6649f98`  |         |            |         |         |
| [Zabo](https://zabo.com/)                                      |   `b6649f98`  |         |            |         |         |
| [Zaif](https://zaif.jp/)                                       |   `b6649f98`  |         |            |         |         |

## Statements

The following public statements have been submitted in response to this CHIP.

### Approve

The following articles have been published in support of this CHIP:

- [Bitcoin Cash Podcast](https://bitcoincashpodcast.com/) (August 7, 2023): [Blocksize Algorithm CHIP endorsement](https://bitcoincashpodcast.com/blog/blocksize-algo-chip-endorsement)
- [General Protocols](https://generalprotocols.com) (September 9, 2023): [General Protocols supports CHIP 2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash](https://read.cash/@GeneralProtocols/general-protocols-supports-chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash-49e2200f)

The following statements have been submitted in support of this CHIP.

>The revisions to this CHIP have made it significantly easier to understand and clearly addresses all major concerns that I've seen raised in regards to this issue.  
>As a Fulcrum server operator, I endorse this proposal. I know I'd be able to keep up with operation costs even if we implemented BIP-101 instead, which would presently have us at 64mb block capacity.  
>I also recognize the urgency of solving this potential social attack before it becomes a problem again. I encourage adoption of this proposal for the November 2023 lock-in, allowing it to be activated in May 2024.

<p align="right">&mdash;
<a href="https://github.com/kzKallisti">kzKallisti</a>,
<a href="https://selene.cash">Selene</a>,
<a href="https://bch.ninja">CashNinja</a>.
</p>

>I am unable to comprehend the math, but I agree with and support the behaviour described and illustrated in various graphs. Further, I am impressed and content with the quality of research done in alternatives and simulating various edge cases and as such I am looking forward to activation of this improvement to managing our max blocksize limit.

<p align="right">&mdash;
<a href="https://gitlab.com/monsterbitar">Jonathan Silverblood</a>,
Bitcoin Cash developer.
</p>

>I (and The Bitcoin Cash Podcast & Selene Wallet) wholly endorse CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash for lock-in 15 November 2023. A carefully selected algorithm that responds to real network demand is an obvious improvement to relieve social burden of discussion around optimal blocksizes plus implementation costs & uncertainty around scaling for miners & node operators. There is also some benefit to the community signalling its commitment to scaling & refusal to repeat the historic delays resulting from previous blocksize increase contention.  
>The amount of work done by bitcoincashautist has been very impressive & inspiring. I refer to not only work on the spec itself but also on iteration from feedback & communicating with stakeholders to patiently address concerns across a variety of mediums. Having reviewed the CHIP thoroughly, I am convinced the chosen parameters accomodate edge cases in a technically sustainable manner.  
>It is a matter of some urgency to lock in this CHIP for November. This will solidify the social contract to scale the BCH blocksize as demand justifies it, all the way to global reserve currency status. Furthermore, it will free up the community zeitgeist to tackle new problems for the 2025 upgrade.  
>A blocksize algorithm implementation is a great step forward for the community. I look forward to this CHIP locking-in in November & going live in May 2024!  

<p align="right">&mdash;
<a href="https://bitcoincashpodcast.com/about">Jeremy</a>,
<a href="https://bitcoincashpodcast.com/">The Bitcoin Cash Podcast</a>,
<a href="https://selene.cash">Selene</a>.
</p>

>Choosing a reasonable blocksize for a global p2p payment system has been an issue of debate in bitcoin for a decade. This CHIP proposal appears to settle the issue by providing a reasonable adjustable default for decades into into the future.  
>Both as research, and the much harder work of communication and exposition, BCA has developed an excellent proposal and presented it clearly to the community according to the accepted process.  
>If BCA's hard work is carried through with careful implementation and extensive testing by node developers, we as a community may look forward to never arguing about blocksize again.  
>' support the Adaptive Blocksize Limit Algorithm for Bitcoin Cash CHIP (8d6081cf) for the May 2024 upgrade.  
>IJHfkednCebJdIGva4Ol0o8qG02qBI3qu7ZNqCPO6mVxeXvKCKyxO0xkYW34s/qb1VKOd66zBt7LB6hPyRGollw=

<p align="right">&mdash;
<a href="https://github.com/2qx">2qx</a>,
Independent Bitcoin Application Developer,
<a href="https://unspent.app">Unspent Phi</a>,
<a href="https://github.com/mainnet-cash/mainnet-js">mainnet-js</a>,
<a href="https://awesomebitcoin.cash">awesomebitcoin.cash</a>,
<a href="https://github.com/2qx/tsugae">bitcoin tsugae</a>.
</p>

>In case it needs to be publicly declared. I personally support CHIP 2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash

<p align="right">&mdash;
<a href="https://twitter.com/CheapLightning">Cheapy</a>,
<a href="https://twitter.com/CheapLightning/status/1708042451032985975">Cat</a>.
</p>

>I fully support this CHIP, It took me quite a while to get through the many components within the chip and algorithm especially since my initial response was: but what if we have exponential adoption? Since if have some background (although limited) in control theory (mainly closed loop PID controllers) this piqued my interest in how the constants are determined in the algorithm. All I can say now is that the only reason not to implement this would be that it would be adding some complexity in a system where I personally admire base layer simplicity, that being said, bitcoincashautist has done an extraordinary job on covering my initial concerns and showing many examples of possible scenarios to the point i’m convinced the positive impact of this upgrade strongly outweighs any negatives I can think of. I really appreciate all the work that has been done on this and fully support it.

<p align="right">&mdash;
<a href="https://bitcoincashresearch.org/t/chip-2023-04-adaptive-blocksize-limit-algorithm-for-bitcoin-cash/1037/114">Kilian</a>,
mechatronics engineer with experience in robotics.
</p>

### Disapprove

### Neutral