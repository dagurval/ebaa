#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <openssl/evp.h>

#define PRNG_USAGE "Usage: ./prng <32-byte hex seed> <number>\n"
int main (int argc, char *argv[])
{
    uint8_t seed[32];
    size_t n;

    if (argc < 3) {
        fprintf(stderr, PRNG_USAGE);
        return 1;
    }
    if (strlen(argv[1]) != 64) {
        fprintf(stderr, "Error, invalid seed length.\n");
        return 1;
    }
    for (size_t i = 0; i < 64; i+=2) {
        if (argv[1][i] >= 'a' && argv[1][i] <= 'f') {
            seed[i/2] = (argv[1][i] - 'a' + 10) * 16;
        }
        else if (argv[1][i] >= '0' && argv[1][i] <= '9') {
            seed[i/2] = (argv[1][i] - '0') * 16;
        }
        else {
            fprintf(stderr, "Error, seed is not a lowercase hex string.\n");
            return 1;
        }
        if (argv[1][i+1] >= 'a' && argv[1][i+1] <= 'f') {
            seed[i/2] += argv[1][i+1] - 'a' + 10;
        }
        else if (argv[1][i+1] >= '0' && argv[1][i+1] <= '9') {
            seed[i/2] += argv[1][i+1] - '0';
        }
        else {
            fprintf(stderr, "Error, seed is not a lowercase hex string.\n");
            return 1;
        }
    }
    if (sscanf(argv[2], "%zu", &n) < 1) {
        fprintf(stderr, "Error, failed reading count.\n");
        return 1;
    }
    fprintf(stderr, "Using seed: ");
    for (size_t i = 0; i < 32; ++i) {
        fprintf(stderr, "%02x", seed[i]);
    }
    fprintf(stderr, "\n");

    EVP_MD_CTX *handle_sha256 = EVP_MD_CTX_new();
    uint8_t result_sha256[32];
    for (size_t i = 0; i < n; ++i) {
        EVP_DigestInit_ex(handle_sha256, EVP_sha256(), NULL);
        EVP_DigestUpdate(handle_sha256, seed, 32);
        EVP_DigestFinal_ex(handle_sha256, result_sha256, NULL);
        uint64_t low64;
        memcpy(&low64, result_sha256, sizeof(low64));
        printf("%" PRIu64 "\n", low64);
        memcpy(seed, result_sha256, 32);
    }
    EVP_MD_CTX_free(handle_sha256);

    return 0;
}
